package com.vitu.riot.champion.web.mapper;

import com.vitu.riot.champion.domain.Champion;
import com.vitu.riot.champion.web.dto.ChampionDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ChampionMapper {

    private final ModelMapper modelMapper;

    public Champion toEntity(ChampionDto championDto) {
        return modelMapper.map(championDto, Champion.class);
    }

    public ChampionDto toDto(Champion champion) {
        return modelMapper.map(champion, ChampionDto.class);
    }
}
