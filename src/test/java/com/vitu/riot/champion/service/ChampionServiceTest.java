package com.vitu.riot.champion.service;

import com.vitu.riot.champion.domain.Champion;
import com.vitu.riot.champion.repository.ChampionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@ExtendWith(SpringExtension.class)
class ChampionServiceTest {

    @Mock
    ChampionRepository championRepository;

    ChampionService championService;

    @BeforeEach
    void setUp() {
        championService = new ChampionService(championRepository);
    }

    @Test
    void findAll() {

        Champion championOne = Champion.builder()
                .name("draven")
                .difficulty("alta")
                .description("arrombado")
                .build();


        Champion championTwo = Champion.builder()
                .name("draven")
                .difficulty("alta")
                .description("arrombado")
                .build();

        List<Champion> expectedChampions = List.of(championOne, championTwo);

        Mockito.when(championRepository.findAll()).thenReturn(expectedChampions);

        List<Champion> champions = championService.findAll();

        Assertions.assertEquals(expectedChampions, champions);

    }

    @Test
    void save() {
        Champion champion = Champion.builder()
                .name("draven")
                .difficulty("alta")
                .description("arrombado")
                .build();

        Champion expectedChampion = Champion.builder()
                .id(1L)
                .name("draven")
                .difficulty("alta")
                .description("arrombado")
                .build();

        Mockito.when(championRepository.save(champion)).thenReturn(expectedChampion);

        Champion save = championService.save(champion);

        Assertions.assertNotNull(save.getId());
        Assertions.assertEquals(champion.getName(), save.getName());
    }
}
