package com.vitu.riot.champion.cucumber.steps;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vitu.riot.champion.web.dto.ChampionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

public abstract class BasicStepDefs {

    @Autowired
    protected MockMvc mockMvc;

    protected ChampionDto championDto;

    protected static MvcResult mvcResult;

    protected String toJson(Object object) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(object);
    }

}
